package dam.android.alejandror.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority = "Normal";

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    // TODO Ex1.3: Variable del EditText per a el place
    private EditText editPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        // TODO Ex1.3: Asignem id al EditText per a poder crear el eventPlace
        editPlace = findViewById(R.id.etEventPlace);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        Bundle inputData = getIntent().getExtras();

        switch (view.getId()) {
            case R.id.btAccept:

                String[] month = getResources().getStringArray(R.array.meses);

                // TODO Ex1.3 Asignem les noves variables demanades
                eventData.putString("EventData", "PLACE: " + editPlace.getText().toString() + "\nPRIORITY: " +
                        priority + "\nDATE: " +dpDate.getDayOfMonth()  + " " +
                        month[dpDate.getMonth()] + " " +
                        dpDate.getYear() + "\nHOUR:" +
                        tpTime.getHour() + ":" + tpTime.getMinute());

                break;
            case R.id.btCancel:
                eventData.putString("EventData", inputData.getString("AnteriorEventData"));
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
