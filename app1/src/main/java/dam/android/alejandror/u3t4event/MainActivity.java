package dam.android.alejandror.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventData;
    private TextView tvCurrentData;

    // TODO Ex1.3: Bundle global per a pasar i tornar informacio
    public Bundle globalBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        etEventData = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);

        tvCurrentData.setText("");

    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);
        globalBundle = new Bundle();

        globalBundle.putString("EventName", etEventData.getText().toString());

        globalBundle.putString("AnteriorEventData", tvCurrentData.getText().toString());
        intent.putExtras(globalBundle);

        startActivityForResult(intent, REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            tvCurrentData.setText(data.getStringExtra("EventData"));
        }

    }

    // TODO Ex1.3: Si girem el dispositiu amb aquest metode RECUPEREM la informacio de tvCurrentData
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        tvCurrentData.setText(savedInstanceState.getString("tvCurrentData"));
    }

    // TODO Ex1.3: Si girem el dispositiu amb aquest metode GUARDEM la informacio de tvCurrentData
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tvCurrentData", tvCurrentData.getText().toString());

    }
}
