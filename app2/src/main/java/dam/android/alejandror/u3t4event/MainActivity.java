package dam.android.alejandror.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventData;
    private TextView tvCurrentData;

    public Bundle globalBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        globalBundle = new Bundle();
        setUI();
    }

    private void setUI() {

        etEventData = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);

        tvCurrentData.setText("");

    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);

        globalBundle.putString("EventName", etEventData.getText().toString());

        // TODO Ex1.2: Guardem la informacio per si en algun cas donem a cancel poder recuperarla després
        globalBundle.putString("AnteriorEventData", tvCurrentData.getText().toString());
        intent.putExtras(globalBundle);

        startActivityForResult(intent, REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST && resultCode == RESULT_OK) {

        }

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        tvCurrentData.setText(savedInstanceState.getString("tvCurrentData"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tvCurrentData", tvCurrentData.getText().toString());

    }
}
