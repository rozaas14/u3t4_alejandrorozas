package dam.android.alejandror.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.CancellationException;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority = "Normal";

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;
    private Button btCalendar;
    private Button btClock;
    private String[] month;



    // TODO Ex4: Variables per a guardar la fetxa elegida dintre de el TimePickerDialog
    private int dia;
    private int mes;
    private int any;
    private TextView fecha;

    // TODO Ex4: Variables per a guardar l'hora elegida dintre de el TimePickerDialog
    private int horaDelDia;
    private int minuts;
    private TextView mostrarHora;


    private EditText editPlace;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);
        btCalendar = findViewById(R.id.btCalendar);
        btClock = findViewById(R.id.btClock);
        editPlace = findViewById(R.id.etEventPlace);
        fecha = findViewById(R.id.tvFecha);
        mostrarHora = findViewById(R.id.tvHora);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);

        btClock.setOnClickListener(this);
        btCalendar.setOnClickListener(this);

        rgPriority.setOnCheckedChangeListener(this);

        month = getResources().getStringArray(R.array.meses);

        calendar();
        time();

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        Bundle inputData = getIntent().getExtras();

        switch (view.getId()) {
            case R.id.btAccept:

                // TODO Ex4: A l'hora de crear el textView anyadairem les noves variables que hem obtes en els dialogs
                eventData.putString("EventData", "PLACE: " + editPlace.getText().toString() + "\nPRIORITY: " + priority + "\nDATE: " + dia + " " +
                        month[mes] + "\nHOUR:" +
                        horaDelDia + ":" + minuts);

                break;
            case R.id.btCancel:
                eventData.putString("EventData", inputData.getString("AnteriorEventData"));
                break;


        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void calendar() {

        final Calendar newCalendar = Calendar.getInstance();
        final DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // TODO Ex4: Obtenim les variables que hem seleccionat el DatePickerDialog
                any = year;
                mes = monthOfYear;
                dia = dayOfMonth;

                fecha.setText(dia + "/" + month[mes] + "/" + any);

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        btCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartTime.show();
            }
        });

    }


    public void time() {

        final Calendar hora = Calendar.getInstance();

        final TimePickerDialog tpDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                // TODO Ex4: Obtenim les variables que hem seleccionat el TimePickerDialog
                horaDelDia = hourOfDay;
                minuts = minute;

                mostrarHora.setText(horaDelDia + ":" + minuts);
            }
        }, hora.get(Calendar.MINUTE), hora.get(Calendar.HOUR), true);

        btClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tpDialog.show();
            }
        });
    }

}
